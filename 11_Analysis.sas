﻿/*---------------------------------------------------------------------------------
Project:            1092

Program:            ____

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
211020	SH		Initial Version
---------------------------------------------------------------------------------*/

* ==================================  Step 1. Import  ================================== *;
proc import out=import1 datafile="F:\1092\Original_Data\211015_ Data Export BSP Created EMS World Expo IO System Questionnaire SH.xlsx" 
	dbms=xlsx replace;
    getnames=yes;
run;
* R = 42, C = 53 *;

proc contents data=import1 position;
run;
/*
Q1			Name:
Q2			Age (years):
Q3			Sex:
Q4			In which of the following regions do you primarily work? - Selected Choice
Q4_6_TEXT	In which of the following regions do you primarily work? - Outside the USA, please specify: - Text
Q5			What is your primary healthcare setting? - Selected Choice
Q5_4_TEXT	What is your primary healthcare setting? - Other, please specify: - Text
Q6			What is your current role? - Selected Choice
Q6_10_TEXT	What is your current role? - Other, please specify:  - Text
Q7			How many years of emergency medical experience do you have?
Q8			Excluding today's training for the simulation, what IO systems(s) have you been previously trained on (select all that apply)? - Selected Choice
Q8_6_TEXT	Excluding today's training for the simulation, what IO systems(s) have you been previously trained on (select all that apply)? - Other, please specify: - Text
Q9			Excluding today's simulation activity, which IO system(s) have you used to gain IO access (select all that apply)? - Selected Choice
Q9_6_TEXT	Excluding today's simulation activity, which IO system(s) have you used to gain IO access (select all that apply)? - Other, please specify:  - Text
Q10			How many years of experience do you have operating IO systems to obtain IO access in patients?
Q11			How many times have you used an IO system for patient care in the past year?
Q12			How often do you utilize the securement/skin attachment (not tape) provided with your IO system to secure the catheter hub after placement?
Q13_1		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - IO Needle Safety Mechanism
Q13_2		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Battery Type (i.e., rechargeable, non-rechargeable)
Q13_3		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Ergonomics
Q13_4		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Securement/Skin Attachment
Q13_5		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Battery Life Indicator (i.e., multi-light, single light)
Q13_6		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Powered Driver Performance
Q13_7		What are the most important product features when choosing a powered IO system to use in your clinical practice? Please drag and drop the features below in order of their importance: 1=most important, and 7=least important - Variety of IO Needle Length Options
Q14			Is there another product feature you would consider when choosing a powered IO system for use in your clinical practice? If yes, where would you rank this among the product features from the previous question? - Selected Choice
Q14_2_TEXT	"Is there another product feature you would consider when choosing a powered IO system for use in your clinical practice? If yes, where would you rank this among the product features from the previous question? - Yes, there is an additional feature.
			Please specify this feature AND provide its order of importance in relation to the features from the previous question: 1-8 (1=most important, 8=least important): - Text"
Q15_1		Please rate your overall satisfaction with the BD's Intraosseous Vascular Access System: - Overall satisfaction
Q16			Please describe your rationale for the satisfaction rating you provided for the BDâ„¢ Intraosseous Vascular Access System:
Q17_1		Please rate your overall satisfaction with the Teleflex EZ-IO System: - Overall satisfaction
Q18			Please describe your rationale for the satisfaction rating you provided for the Teleflex EZ-IO System:
Q19_1		IO Needle Safety: - BD's Intraosseous Vascular Access System
Q19_2		IO Needle Safety: - Teleflex EZ-IO System
Q20_1		IO Needle Performance: - BD's Intraosseous Vascular Access System
Q20_2		IO Needle Performance: - Teleflex EZ-IO System
Q21_1		Variety of IO Needle Length Options: - BDâ„¢ Intraosseous Vascular Access System
Q21_2		Variety of IO Needle Length Options: - Teleflex EZ-IO System
Q22_1		Powered Driver Performance: - BD's Intraosseous Vascular Access System
Q22_2		Powered Driver Performance: - Teleflex EZ-IO System
Q23_1		Powered Driver Ergonomics: - BD's Intraosseous Vascular Access System
Q23_2		Powered Driver Ergonomics: - Teleflex EZ-IO System
Q24_1		Battery Life Indicator (i.e., multi-light, single light): - BD's Intraosseous Vascular Access System
Q24_2		Battery Life Indicator (i.e., multi-light, single light): - Teleflex EZ-IO System
Q25_1		Battery Type (i.e., rechargeable, non-rechargeable): - BD Intraosseous Vascular Access System
Q25_2		Battery Type (i.e., rechargeable, non-rechargeable): - Teleflex EZ-IO System
Q26_1		Securement/Skin Attachment: - BD's Intraosseous Vascular Access System
Q26_2		Securement/Skin Attachment: - Teleflex EZ-IO System
Q27_1		Overall IO System Ease-of-Use: - BD's Intraosseous Vascular Access System
Q27_2		Overall IO System Ease-of-Use: - Teleflex EZ-IO System
Q28_1		Overall IO System Safety: - BD's Intraosseous Vascular Access System
Q28_2		Overall IO System Safety: - Teleflex EZ-IO System
Q29			Please provide general feedback on the BD's Intraosseous Vascular Access System, including things that can be improved with the device/handling:
Q30			Would you be interested in participating in future research regarding the BD's Intraosseous Vascular Access System? If yes, please provide your contact information. - Selected Choice
Q30_1_TEXT	Would you be interested in participating in future research regarding the BD's Intraosseous Vascular Access System? If yes, please provide your contact information. - Yes. Please provide your contact information: - Text
*/



* ==================================  Step 2. Create ID  ================================== *;
proc sql;
	select n(distinct Q1) as N
	from import1;
quit;
* N = 42 *;

data import2 (drop=Q1);
	set import1;
	ID = _n_;
run;
* R = 42 *;



* ==================================  Step 3. Demographics  ================================== *;
proc sql;
	create table demo1 as
	select  ID,

			Q2,

			Q3,

			Q4,
			Q4_6_TEXT,

			Q5,
			Q5_4_TEXT,

			Q6,
			Q6_10_TEXT,

			Q7,

			Q8,
			Q8_6_TEXT,

			Q9,
			Q9_6_TEXT,

			Q10,

			Q11,

			Q12
	from import2;
	order by ID;
quit;

proc means data=demo1 n nmiss min max;
run;

proc means data=demo1 n nmiss mean std median q1 q3 min max;
	var Q2 Q7 Q10;
run;

proc freq data=demo1;
	table Q9 / missing norow nocol nocum;
run; 
/*
Q3
Q4
Q5
Q6
Q8
Q9
Q11
Q12
*/

proc freq data=demo1;
	*table Q4 * Q4_6_TEXT / missing norow nocum nopercent;
	*table Q5 * Q5_4_TEXT / missing norow nocum nopercent;
	*table Q6 * Q6_10_TEXT / missing norow nocum nopercent;
	*table Q8 * Q8_6_TEXT / missing norow nocum nopercent;
	table Q9 * Q9_6_TEXT / missing norow nocum nopercent;
run; 

data zzz1 (keep=Q11);
	set demo1;
	*if Q11 in ('44237', '44520');
run;

data demo2;
	set demo1;
	if Q11 = '44237' then Q11 = '2-10'; /* 44237 came from Feb-10 */
	if Q11 = '44520' then Q11 = '11-20'; /* 44520 came from Nov-20 */
run; 
   
proc freq data=demo2;
	table Q11 / missing norow nocol nocum;
run; 



* ==================================  Step 4. Q13: Product Features  ================================== *;
proc sql;
	create table feature1 as
	select  ID,

			Q13_1,
			Q13_2,
			Q13_3,
			Q13_4,
			Q13_5,
			Q13_6,
			Q13_7
	from import2;
	order by ID;
quit;
* R = 42 *;

proc freq data=feature1;
	table Q13_: / missing norow nocol nocum;
run; 

data feature2;
	set feature1;
	if Q13_1 = 1 then Q13_1 = 100 * (6/6);
	else if Q13_1 = 2 then Q13_1 = 100 * (5/6);
	else if Q13_1 = 3 then Q13_1 = 100 * (4/6);
	else if Q13_1 = 4 then Q13_1 = 100 * (3/6);
	else if Q13_1 = 5 then Q13_1 = 100 * (2/6);
	else if Q13_1 = 6 then Q13_1 = 100 * (1/6);
	else if Q13_1 = 7 then Q13_1 = 100 * (0/6);

	if Q13_2 = 1 then Q13_2 = 100 * (6/6);
	else if Q13_2 = 2 then Q13_2 = 100 * (5/6);
	else if Q13_2 = 3 then Q13_2 = 100 * (4/6);
	else if Q13_2 = 4 then Q13_2 = 100 * (3/6);
	else if Q13_2 = 5 then Q13_2 = 100 * (2/6);
	else if Q13_2 = 6 then Q13_2 = 100 * (1/6);
	else if Q13_2 = 7 then Q13_2 = 100 * (0/6);

	if Q13_3 = 1 then Q13_3 = 100 * (6/6);
	else if Q13_3 = 2 then Q13_3 = 100 * (5/6);
	else if Q13_3 = 3 then Q13_3 = 100 * (4/6);
	else if Q13_3 = 4 then Q13_3 = 100 * (3/6);
	else if Q13_3 = 5 then Q13_3 = 100 * (2/6);
	else if Q13_3 = 6 then Q13_3 = 100 * (1/6);
	else if Q13_3 = 7 then Q13_3 = 100 * (0/6);

	if Q13_4 = 1 then Q13_4 = 100 * (6/6);
	else if Q13_4 = 2 then Q13_4 = 100 * (5/6);
	else if Q13_4 = 3 then Q13_4 = 100 * (4/6);
	else if Q13_4 = 4 then Q13_4 = 100 * (3/6);
	else if Q13_4 = 5 then Q13_4 = 100 * (2/6);
	else if Q13_4 = 6 then Q13_4 = 100 * (1/6);
	else if Q13_4 = 7 then Q13_4 = 100 * (0/6);

	if Q13_5 = 1 then Q13_5 = 100 * (6/6);
	else if Q13_5 = 2 then Q13_5 = 100 * (5/6);
	else if Q13_5 = 3 then Q13_5 = 100 * (4/6);
	else if Q13_5 = 4 then Q13_5 = 100 * (3/6);
	else if Q13_5 = 5 then Q13_5 = 100 * (2/6);
	else if Q13_5 = 6 then Q13_5 = 100 * (1/6);
	else if Q13_5 = 7 then Q13_5 = 100 * (0/6);

	if Q13_6 = 1 then Q13_6 = 100 * (6/6);
	else if Q13_6 = 2 then Q13_6 = 100 * (5/6);
	else if Q13_6 = 3 then Q13_6 = 100 * (4/6);
	else if Q13_6 = 4 then Q13_6 = 100 * (3/6);
	else if Q13_6 = 5 then Q13_6 = 100 * (2/6);
	else if Q13_6 = 6 then Q13_6 = 100 * (1/6);
	else if Q13_6 = 7 then Q13_6 = 100 * (0/6);

	if Q13_7 = 1 then Q13_7 = 100 * (6/6);
	else if Q13_7 = 2 then Q13_7 = 100 * (5/6);
	else if Q13_7 = 3 then Q13_7 = 100 * (4/6);
	else if Q13_7 = 4 then Q13_7 = 100 * (3/6);
	else if Q13_7 = 5 then Q13_7 = 100 * (2/6);
	else if Q13_7 = 6 then Q13_7 = 100 * (1/6);
	else if Q13_7 = 7 then Q13_7 = 100 * (0/6);
run;

proc means data=feature2 n nmiss mean std median q1 q3 min max;
	var Q13_:;
run;



* ==================================  Step 5. Q14: Rank  ================================== *;
proc sql;
	create table rank1 as
	select  ID,

			Q14,
			Q14_2_TEXT
	from import2;
	order by ID;
quit;
* R = 42 *;

proc freq data=rank1;
	table Q14 / missing norow nocol nocum;
run; 

proc freq data=rank1;
	table Q14_2_TEXT * Q14 / missing norow nocum nopercent;
run; 



* ==================================  Step 6. Comparisons  ================================== *;
proc sql;
	create table comparison1 as
	select  ID,

			Q15_1,
			Q17_1,

			Q16,
			Q18,

			Q19_1,
			Q19_2,

			Q20_1,
			Q20_2,

			Q21_1,
			Q21_2,

			Q22_1,
			Q22_2,

			Q23_1,
			Q23_2,

			Q24_1,
			Q24_2,

			Q25_1,
			Q25_2,

			Q26_1,
			Q26_2,

			Q27_1,
			Q27_2,

			Q28_1,
			Q28_2
	from import2
	order by ID;
quit;
* R = 42 *;

proc means data=comparison1 n nmiss min max;
run;

data comparison2;
	set comparison1;
	Q15_1 = Q15_1 * 10;
	Q17_1 = Q17_1 * 10;

	Q19_1 = Q19_1 * 10;
	Q19_2 = Q19_2 * 10;

	Q20_1 = Q20_1 * 10;
	Q20_2 = Q20_2 * 10;

	Q21_1 = Q21_1 * 10;
	Q21_2 = Q21_2 * 10;

	Q22_1 = Q22_1 * 10;
	Q22_2 = Q22_2 * 10;

	Q23_1 = Q23_1 * 10;
	Q23_2 = Q23_2 * 10;

	Q24_1 = Q24_1 * 10;
	Q24_2 = Q24_2 * 10;

	Q25_1 = Q25_1 * 10;
	Q25_2 = Q25_2 * 10;

	Q26_1 = Q26_1 * 10;
	Q26_2 = Q26_2 * 10;

	Q27_1 = Q27_1 * 10;
	Q27_2 = Q27_2 * 10;

	Q28_1 = Q28_1 * 10;
	Q28_2 = Q28_2 * 10;
run;

proc means data=comparison2 n nmiss min max;
run;

proc means data=comparison2 n nmiss mean std median q1 q3 min max;
run;

/*proc ttest data=comparison2;
  	*paired Q15_1 * Q17_1;
  	*paired Q19_1 * Q19_2;
  	*paired Q20_1 * Q20_2;
  	*paired Q21_1 * Q21_2;
  	*paired Q22_1 * Q22_2;
  	*paired Q23_1 * Q23_2;
  	*paired Q24_1 * Q24_2;
  	*paired Q25_1 * Q25_2;
  	*paired Q26_1 * Q26_2;
  	*paired Q27_1 * Q27_2;
  	paired Q28_1 * Q28_2;
run;*/

data comparison3;
  	set comparison2;
  	diff1 = Q15_1 - Q17_1;
  	diff2 = Q19_1 - Q19_2;
  	diff3 = Q20_1 - Q20_2;
  	diff4 = Q21_1 - Q21_2;
  	diff5 = Q22_1 - Q22_2;
  	diff6 = Q23_1 - Q23_2;
  	diff7 = Q24_1 - Q24_2;
  	diff8 = Q25_1 - Q25_2;
  	diff9 = Q26_1 - Q26_2;
  	diff10 = Q27_1 - Q27_2;
  	diff11 = Q28_1 - Q28_2;
run;

proc univariate data=comparison3;
  	var diff11;
run;
/*
Paired t-test
A paired (samples) t-test is used when you have two related observations (i.e., two observations per subject) and you want to see if the means on these two normally distributed interval variables differ from one another. 

Wilcoxon signed rank sum test
The Wilcoxon signed rank sum test is the non-parametric version of a paired samples t-test.
*/

**************************************************  End  **************************************************;
